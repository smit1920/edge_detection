all: process process_loop process_omp

main_process.o: main_process.c
	gcc -O3 -c main_process.c 

loop_reordered_process.o: loop_reordered_process.c
	gcc -O3 -c loop_reordered_process.c

openmp_process.o: openmp_process.c
	gcc -O3 -c -fopenmp openmp_process.c

png_util.o: png_util.c
	gcc -l lpng16 -c png_util.c

process: main_process.o png_util.o
	gcc -o process -lm -l png16 main_process.o png_util.o

process_loop: loop_reordered_process.o png_util.o
	gcc -o process_loop -lm -l png16 loop_reordered_process.o png_util.o

process_omp: openmp_process.o png_util.o
	gcc -fopenmp -o process_omp -lm -l png16 openmp_process.o png_util.o

test: process process_loop process_omp
	./process ./images/cube.png test.png
	./process_loop ./images/cube.png test_loop.png
	./process_omp ./images/cube.png test_omp.png

clean:
	rm *.o
	rm process
	rm process_loop
	rm process_omp
